The packaging is kept https://salsa.debian.org/brentclark-guest/clamav-unofficial-sigs

Merging major upstream releases
========
Please note that the following files need to be manually extracted and updated for packaging:
 clamav-unofficial-sigs.8
 clamav-unofficial-sigs-cron
 clamav-unofficial-sigs-logrotate

The reason for this is 'clamav-unofficial-sigs.sh' has the above files 'baked' in the script itself.

It is recommend to have a testing environment and then follow their INSTALL. When you are ready to '--install-man', '--install-logrotate'
and '--install-cron', make a note of the location, where 'clamav-unofficial-sigs.sh' installs the above files and then add them to your package.
