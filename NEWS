# clamav-unofficial-sigs [![GitHub Release](https://img.shields.io/github/release/extremeshok/clamav-unofficial-sigs.svg?label=Latest)](https://github.com/extremeshok/clamav-unofficial-sigs/releases/latest) [![Build Status](https://travis-ci.org/extremeshok/clamav-unofficial-sigs.svg?branch=master)](https://travis-ci.org/extremeshok/clamav-unofficial-sigs) [![Issue Count](https://codeclimate.com/github/extremeshok/clamav-unofficial-sigs/badges/issue_count.svg)](https://codeclimate.com/github/extremeshok/clamav-unofficial-sigs)

ClamAV Unofficial Signatures Updater

Github fork of the sourceforge hosted and non maintained utility.

## Maintained and provided by https://eXtremeSHOK.com

## Description
The clamav-unofficial-sigs script provides a simple way to download, test, and update third-party signature databases provided by Sanesecurity, FOXHOLE, OITC, Scamnailer, BOFHLAND, CRDF, Porcupine, Securiteinfo, MalwarePatrol, Yara-Rules Project, etc. The script will also generate and install cron, logrotate, and man files.

#### Try our custom spamassasin plugin: https://github.com/extremeshok/spamassassin-extremeshok_fromreplyto

### Support / Suggestions / Comments
Please post them on the issue tracker : https://github.com/extremeshok/clamav-unofficial-sigs/issues

### Submit Patches / Pull requests to the "Dev" Branch

### Required Ports / Firewall Exceptions
* rsync: TCP port 873
* wget/curl : TCP port 443

### Supported Operating Systems
Debian, Ubuntu, Raspbian, CentOS (RHEL and clones), OpenBSD, FreeBSD, OpenSUSE, Archlinux, Mac OS X, Slackware, Solaris (Sun OS), pfSense, Zimbra and derivative systems  

### Quick Install Guide
* Download the files to /tmp/
* Copy clamav-unofficial-sigs.sh to /usr/local/sbin/
* Set 755 permissions on  /usr/local/sbin/clamav-unofficial-sigs.sh
* Make the directory /etc/clamav-unofficial-sigs/
* Copy the contents of config/ into /etc/clamav-unofficial-sigs/
* Make the directory /var/log/clamav-unofficial-sigs/
* Rename the your os.your-distro.conf to os.conf, where your-distro is your distribution
* Set your user config options in the configs /etc/clamav-unofficial-sigs/user.conf
* Run the script with --install-cron to install the cron file
* Run the script with --install-logrotate to install the logrotate file
* Run the script with --install-man to install the man file

### First Usage
* Run the script once as your superuser to set all the permissions and create the relevant directories

### Systemd
* Copy the contents of systemd/ into to /etc/systemd/

### Advanced Config Overrides
* Default configs are loaded in the following order if they exist:
* master.conf -> os.conf -> user.conf or your-specified.config
* user.conf will override os.conf and master.conf, os.conf will override master.conf
* A minimum of 1 config is required.
* Specifying a config on the command line (-c | --config) will override the loading of the default configs

#### Check if signature are being loaded
**Run the following command to display which signatures are being loaded by clamav

```clamscan --debug 2>&1 /dev/null | grep "loaded"```

#### SELinux cron permission fix
> WARNING - Clamscan reports ________ database integrity tested BAD - SKIPPING

**Run the following command to allow clamav selinux support**

```setsebool -P antivirus_can_scan_system true```

### Yara Rule Support automatically enabled (as of April 2016)
Since usage yara rules requires clamav 0.99 or above, they will be automatically deactivated if your clamav is older than the required version

### Yara-Rules Project Support (as of June 2015)
Usage of free Yara-Rules Project: http://yararules.com
- Enabled by default

Current limitations of clamav support : http://blog.clamav.net/search/label/yara

### MalwarePatrol Free/Delayed list support (as of May 2015)
Usage of MalwarePatrol 2015 free clamav signatures : https://www.malwarepatrol.net
 - 1. Sign up for a free account : https://www.malwarepatrol.net/signup-free.shtml
 - 2. You will recieve an email containing your password/receipt number
 - 3. Enter the receipt number into the config malwarepatrol_receipt_code: replacing YOUR-RECEIPT-NUMBER with your receipt number from the email

### SecuriteInfo Free/Delayed list support (as of June 2015)
Usage of SecuriteInfo 2015 free clamav signatures : https://www.securiteinfo.com
 - 1. Sign up for a free account : https://www.securiteinfo.com/clients/customers/signup
 - 2. You will recieve an email to activate your account and then a followup email with your login name
 - 3. Login and navigate to your customer account : https://www.securiteinfo.com/clients/customers/account
 - 4. Click on the Setup tab
 - 5. You will need to get your unique identifier from one of the download links, they are individual for every user
 - 5.1. The 128 character string is after the http://www.securiteinfo.com/get/signatures/
 - 5.2. Example https://www.securiteinfo.com/get/signatures/your_unique_and_very_long_random_string_of_characters/securiteinfo.hdb
   Your 128 character authorisation signature would be : your_unique_and_very_long_random_string_of_characters
 - 6. Enter the authorisation signature into the config securiteinfo_authorisation_signature: replacing YOUR-SIGNATURE-NUMBER with your authorisation signature from the link

### Linux Malware Detect support (as of May 2015)
Usage of free Linux Malware Detect clamav signatures: https://www.rfxn.com/projects/linux-malware-detect/
 - Enabled by default, no configuration required

## USAGE

Usage: clamav-unofficial-sigs.sh [OPTION] [PATH|FILE]

-c, --config    Use a specific configuration file or directory
        eg: '-c /your/dir' or ' -c /your/file.name'
        Note: If a directory is specified the directory must contain atleast:
        master.conf, os.conf or user.conf
        Default Directory: /etc/clamav-unofficial-sigs

-F, --force     Force all databases to be downloaded, could cause ip to be blocked

-h, --help      Display this script's help and usage information

-V, --version   Output script version and date information

-v, --verbose   Be verbose, enabled when not run under cron

-s, --silence   Only output error messages, enabled when run under cron

-d, --decode-sig        Decode a third-party signature either by signature name
        (eg: Sanesecurity.Junk.15248) or hexadecimal string.
        This flag will 'NOT' decode image signatures

-e, --encode-string     Hexadecimal encode an entire input string that can
        be used in any '*.ndb' signature database file

-f, --encode-formatted  Hexadecimal encode a formatted input string containing
        signature spacing fields '{}, (), *', without encoding
        the spacing fields, so that the encoded signature
        can be used in any '*.ndb' signature database file

-g, --gpg-verify        GPG verify a specific Sanesecurity database file
        eg: '-g filename.ext' (do not include file path)

-i, --information       Output system and configuration information for
        viewing or possible debugging purposes

-m, --make-database     Make a signature database from an ascii file containing
        data strings, with one data string per line.  Additional
        information is provided when using this flag

-t, --test-database     Clamscan integrity test a specific database file
        eg: '-t filename.ext' (do not include file path)

-o, --output-triggered  If HAM directory scanning is enabled in the script's
        configuration file, then output names of any third-party
        signatures that triggered during the HAM directory scan

-w, --whitelist Adds a signature whitelist entry in the newer ClamAV IGN2
        format to 'my-whitelist.ign2' in order to temporarily resolve
        a false-positive issue with a specific third-party signature.
        Script added whitelist entries will automatically be removed
        if the original signature is either modified or removed from
        the third-party signature database

--check-clamav  If ClamD status check is enabled and the socket path is correctly
        specifiedthen test to see if clamd is running or not

--install-all   Install and generate the cron, logroate and man files, autodetects the values
         based on your config files

--install-cron  Install and generate the cron file, autodetects the values
        based on your config files

--install-logrotate     Install and generate the logrotate file, autodetects the
        values based on your config files

--install-man   Install and generate the man file, autodetects the
         values based on your config files

--remove-script     Remove the clamav-unofficial-sigs script and all of
        its associated files and databases from the system      
